#!/bin/bash
set -e

ACTION="backup"
export BACKUP_BUCKET_NAME=${BACKUP_BUCKET_NAME-gitlab-backups}
export BACKUP_BACKEND=${BACKUP_BACKEND-s3}
S3_CMD_BACKUP_OPTION=""

rails_dir=/srv/gitlab
backups_path=$rails_dir/tmp/backups
backup_tars_path=$rails_dir/tmp/backup_tars
object_storage_backends=( registry uploads artifacts lfs packages external_diffs terraform_state )

skipping_backup_for=()

function usage()
{
  cat << HEREDOC

   Usage: backup-utility [--restore] [-f URL] [-t TIMESTAMP] [--skip COMPONENT] [--backend BACKEND] [--s3config CONFIG]

   Options:
     -h, --help                             Show this help message and exit.
     --restore [-t TIMESTAMP | -f URL]      When specified, utility restores from an existing backup specified
                                            as url or timestamp in object storage.
     -f URL                                 http(s):/ftp:/file: URL with backup location. Use with --restore.
     -t TIMESTAMP                           Timestamp (part before '_gitlab_backup.tar' in archive name),
                                            can be used to specify backup source or target name.
     --rsyncable                            Pass the '--rsyncable' parameter to gzip for artifact compression.
     --skip COMPONENT                       When specified, utility will skip the backup of COMPONENT.
                                            May be defined multiple times. Valid values for COMPONENT are
                                            db, repositories, and any of the object storages (e.g. 'lfs').
     --backend BACKEND                      Object storage backend to use for backups.
                                            Can be either 's3' or 'gcs'.
     --s3config CONFIG                      S3 backend configuration to use for backups storage.
                                            Special config file for s3cmd (see: https://s3tools.org/usage)
     --storage-class CLASSNAME              Pass this storage class to the gcs or s3cmd for more cost-efficient
                                            storage of backups.
HEREDOC
}

# Checks if provided argument is a url for downloading it
function is_url() {
  regex='(https?|ftp|file)://[-A-Za-z0-9\+&@#/%?=~_|!:,.;]*[-A-Za-z0-9\+&@#/%=~_|]'

  [[ $1 =~ $regex ]]
}

function fetch_remote_backup(){
  mkdir -p $backups_path
  output_path=$backups_path/0_gitlab_backup.tar

  if is_url $1; then
    >&2 echo "Downloading from $1";
    curl --retry 6 --progress-bar -o $output_path $1
  else # It's a timestamp
    file_name="$1_gitlab_backup.tar"
    if [ "${BACKUP_BACKEND}" = "s3" ]; then
      s3cmd ${S3_CMD_BACKUP_OPTION} get "s3://$BACKUP_BUCKET_NAME/$file_name" $output_path > /dev/null
    elif [ "${BACKUP_BACKEND}" = "gcs" ]; then
      gsutil cp "gs://$BACKUP_BUCKET_NAME/$file_name" $output_path > /dev/null
    else
      echo "Unknown backend: ${BACKUP_BACKEND}"
    fi
  fi
  echo $output_path
}

function unpack_backup(){
  local file_path=$1
  cd $(dirname $file_path)

  echo "Unpacking backup"

  if [ ! -f $file_path ]; then
    echo $file_path not found
    exit 1
  fi

  tar -xf $file_path
}

function pack_backup(){
  echo "Packing up backup tar"
  local backup_name=$1
  tar -cf ${backup_tars_path}/${backup_name}.tar -C $backups_path .
}

function get_version(){
  cat $rails_dir/VERSION
}

function get_backup_name(){
  if [ -n "$BACKUP_TIMESTAMP" ]; then
    echo ${BACKUP_TIMESTAMP}_gitlab_backup
  else
    now_timestamp=$(date +%s_%Y_%m_%d)
    gitlab_version=$(get_version)
    echo ${now_timestamp}_${gitlab_version}_gitlab_backup
  fi
}

function cleanup(){
  rm -rf $backups_path/*
  rm -rf $backup_tars_path/*
}

function write_backup_info(){
  cat << EOF > $backups_path/backup_information.yml
:db_version: $($rails_dir/bin/rails runner "File.write('/tmp/db_version', ActiveRecord::Migrator.current_version.to_s)" && cat /tmp/db_version)
:backup_created_at: $(date "+%Y-%m-%d %H:%M:%S %z")
:gitlab_version: $(get_version)
:tar_version: $(tar --version | head -n 1)
:installation_type: gitlab-helm-chart
:skipped: $1
EOF
}

function get_skipped(){
  all=( builds.tar.gz db repositories pages.tar.gz )
  for storage in ${object_storage_backends[@]}; do
    all+=( "${storage}.tar.gz" );
  done;
  skipped_string=""

  for backup_item in ${all[@]}; do
    if [ ! -e $backups_path/$backup_item ]; then
      skipped_string="$skipped_string,${backup_item%.tar.gz}";
    fi;
  done;

  echo ${skipped_string#,}
}

function backup(){
  backup_name=$(get_backup_name)
  mkdir -p $backup_tars_path

  if ! [[ ${skipping_backup_for[@]} =~ "db" ]]; then
    gitlab-rake gitlab:backup:db:create
  fi
  if ! [[ ${skipping_backup_for[@]} =~ "repositories" ]]; then
    gitlab-rake gitlab:backup:repo:create
  fi

  for backup_item in ${object_storage_backends[@]}; do
    if ! [[ ${skipping_backup_for[@]} =~ $backup_item ]]; then
      object-storage-backup $backup_item $backups_path/${backup_item}.tar.gz
    fi
  done

  skipped=$(get_skipped $backup_name)
  write_backup_info $skipped
  pack_backup $backup_name
  if [ "${BACKUP_BACKEND}" = "s3" ]; then
    if [ -z "${STORAGE_CLASS}" ]; then
      s3cmd ${S3_CMD_BACKUP_OPTION} put ${backup_tars_path}/${backup_name}.tar s3://$BACKUP_BUCKET_NAME > /dev/null
    else
      s3cmd ${S3_CMD_BACKUP_OPTION} put --storage-class "${STORAGE_CLASS}" ${backup_tars_path}/${backup_name}.tar s3://$BACKUP_BUCKET_NAME > /dev/null
    fi
    echo "[DONE] Backup can be found at s3://$BACKUP_BUCKET_NAME/${backup_name}.tar"
  elif [ "${BACKUP_BACKEND}" = "gcs" ]; then
    if [ -z "${STORAGE_CLASS}" ]; then
      gsutil cp -n ${backup_tars_path}/${backup_name}.tar gs://$BACKUP_BUCKET_NAME > /dev/null
    else
      gsutil cp -s "${STORAGE_CLASS}" -n ${backup_tars_path}/${backup_name}.tar gs://$BACKUP_BUCKET_NAME > /dev/null
    fi
    echo "[DONE] Backup can be found at gs://$BACKUP_BUCKET_NAME/${backup_name}.tar"
  else
    echo "Unknown backend for backup: ${BACKUP_BACKEND}"
  fi

  cleanup
}

function is_skipped() {
  [[ $SKIPPED =~ $1 ]]
}

function restore(){
  if [ -z "$BACKUP_URL" ] && [ -z "$BACKUP_TIMESTAMP" ]; then
    echo "You need to set BACKUP_URL or BACKUP_TIMESTAMP variable"
    exit 1
  fi

  BACKUP=${BACKUP_URL-}
  if [ -z "$BACKUP" ]; then
    BACKUP=$BACKUP_TIMESTAMP
  fi

  file=$(fetch_remote_backup $BACKUP)

  dir_name=$(dirname $file)
  file_name=$(basename $file)
  timestamp="${file_name%%_*}"
  export BACKUP=$timestamp
  unpack_backup $file

  skipped_line=$(grep skipped $(dirname $file)/backup_information.yml)
  export SKIPPED=$(echo ${skipped_line#:skipped:})

  installation_type_line=$(grep installation_type $(dirname $file)/backup_information.yml || echo ":installation_type: unknown")
  export INSTALLATION_TYPE=$(echo ${installation_type_line#:installation_type: })

  ! is_skipped "db"           && gitlab-rake gitlab:db:drop_tables
  ! is_skipped "db"           && gitlab-rake gitlab:backup:db:restore

  # Previous versions of the dump failed to mark the repos as skipped, so we additionally check for the directory
  if [ -e $backups_path/repositories ]; then
    ! is_skipped "repositories" && gitlab-rake gitlab:backup:repo:restore
  fi

  ! is_skipped "builds"       && gitlab-rake gitlab:backup:builds:restore

  if [ "$INSTALLATION_TYPE" = "gitlab-helm-chart" ]; then
    for restore_item in ${object_storage_backends[@]}; do
      if [ -f $backups_path/${restore_item}.tar.gz ]; then
        ! is_skipped $restore_item && object-storage-restore $restore_item $backups_path/${restore_item}.tar.gz
      fi
    done
  else
    echo "Backup tarball not from a Helm chart based installation. Not processing files in object storage."
  fi

  gitlab-rake cache:clear
}

while [[ $# -gt 0 ]]
do
  key="$1"

  case $key in
    -h|--help)
      usage
      ACTION="none"
      break
      ;;
    -f|--file)
      BACKUP_URL="$2"
      shift
      shift
      ;;
    -t|--timestamp)
      BACKUP_TIMESTAMP="$2"
      shift
      shift
      ;;
    --backend)
      export BACKUP_BACKEND="$2"
      shift
      shift
      ;;
    --s3config)
      if [ ! -f $2 ]; then
        echo "s3cmd file specified does not exist";
        exit 1;
      fi
      export S3_CMD_BACKUP_OPTION="--config=$2 "
      shift
      shift
      ;;
    --restore)
      ACTION="restore"
      shift
      ;;
    --rsyncable)
      export GZIP_RSYNCABLE="yes"
      shift
      ;;
    --skip)
      skipping_backup_for+=( "$2" )
      shift
      shift
      ;;
    --storage-class)
      export STORAGE_CLASS="$2"
      shift
      shift
      ;;
    *)
      usage
      echo "Unexpected parameter: $key"
      exit 1
      ;;
  esac
done

if [ "$ACTION" = "restore" ]; then
  restore
elif [ "$ACTION" = "backup" ]; then
  backup
fi
